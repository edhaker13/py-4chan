"""
    py-4chan
    ~~~~~~~~

    Python Wrappers to access 4chan's API.

    This program is free software. It comes without any warranty, to
    the extent permitted by applicable law. You can redistribute it
    and/or modify it under the terms of the Do What The Fuck You Want
    To Public License, Version 2, as published by Sam Hocevar. See
    http://sam.zoy.org/wtfpl/COPYING for more details.

"""

from datetime import datetime
import base64

import requests


_4CHAN_API = 'api.4chan.org'
_4CHAN_BOARDS_URL = 'boards.4chan.org'
_4CHAN_IMAGES_URL = 'images.4chan.org'
_4CHAN_THUMBS_URL = '0.thumbs.4chan.org'
_BOARD = '%s/%i.json'
_THREAD = '%s/res/%i.json'
_VERSION = '0.1.4'


class Board(object):
    def __init__(self, boardName, https=False, apiUrl=_4CHAN_API, session=None):
        self._https = https
        self._base_url = ('http://' if not https else 'https://') + apiUrl
        self._board_name = boardName
        self._session = session or requests.session(headers={'User-Agent': 'py-4chan/%s' % _VERSION})
        self._thread_cache = {}

    def get_thread(self, thread_id, update_if_cached=True):
        """
            Get a thread from 4chan via 4chan API.

            :param thread_id: thread ID
            :param update_if_cached: Should the thread be updated if it's found in the cache?
            :return: Thread
        """
        if thread_id in self._thread_cache:
            thread = self._thread_cache[thread_id]
            if update_if_cached:
                thread.update()

            return thread

        url = '%s/%s' % (self._base_url, _THREAD % (self._board_name, thread_id))
        res = self._session.get(url)

        thread = Thread._from_request(self, res, thread_id)
        self._thread_cache[thread_id] = thread

        return thread

    def thread_exists(self, thread_id):
        """
            Check if a thread exists, or is 404.
            :param thread_id: thread ID
            :return: bool
        """
        url = '%s/%s' % (self._base_url, _THREAD % (self._board_name, thread_id))
        return self._session.head(url).status_code == 200

    def get_threads(self, page=1):
        """
            Get thread on pages, if the thread is already in cache, return cached thread entry.

            Sets thread.want_update to True if the thread is being returned from the cache.
            :param: page: page to fetch thread from
            :return: list[Thread]
        """
        page -= 1
        url = '%s/%s' % (self._base_url, _BOARD % (self._board_name, page))
        response = self._session.get(url)
        if response.status_code != 200:
            response.raise_for_status()

        json = response.json
        threads = []
        for thread_json in json['threads']:
            thread_id = thread_json['posts'][0]['no']
            if thread_id in self._thread_cache:
                thread = self._thread_cache[thread_id]
                thread.want_update = True
            else:
                thread = Thread._from_json(thread_json, self)
                self._thread_cache[thread.id] = thread

            threads.append(thread)

        return threads


    @property
    def name(self):
        """
            Board name that this board represents.
            :return: string
        """
        return self._board_name


class Thread(object):
    def __init__(self, board, thread_id):
        self._board = board
        self.id = thread_id
        self.topic = None
        self.replies = []
        self.is_404 = False
        self.last_reply_id = 0
        self.omitted_posts = 0
        self.omitted_images = 0
        self.want_update = False
        self._last_modified = None

    @property
    def closed(self):
        """
            Is the thread closed?
            :return: bool
        """
        return self.topic._data['closed'] == 1

    @property
    def sticky(self):
        """
            Is the thread sticky?
            :return: bool
        """
        return self.topic._data['sticky'] == 1

    @staticmethod
    def _from_request(board, response, thread_id):
        if response.status_code == 404:
            return None

        elif response.status_code == 200:
            return Thread._from_json(response.json, board, thread_id, response.headers['last-modified'])

        else:
            response.raise_for_status()

    @staticmethod
    def _from_json(json, board, thread_id=None, last_modified=None):
        t = Thread(board, thread_id)
        t._last_modified = last_modified

        posts = json['posts']

        t.topic = Post(t, posts[0])
        t.replies.extend(Post(t, p) for p in posts[1:])

        if thread_id is not None:
            if not t.replies:
                t.last_reply_id = t.topic.post_number
            else:
                t.last_reply_id = t.replies[-1].post_number

        else:
            t.want_update = True
            head = posts[0]
            t.id = head['no']
            try:
                t.omitted_images = head['omitted_images']
                t.omitted_posts = head['omitted_posts']

            except KeyError:
                pass

        return t


    def get_files(self):
        """
            Returns a generator that yields all the URL of all the files (not thumbnails) in the thread.
        """
        yield self.topic.file_url
        for reply in self.replies:
            if reply.has_file:
                yield reply.file_url

    def get_thumbs(self):
        """
            Returns a generator that yields all the URL of all the thumbnails in the thread.
        """
        yield self.topic.thumbnail_url
        for reply in self.replies:
            if reply.has_file:
                yield reply.thumbnail_url

    def update(self, force=False):
        """
            Fetch new posts from the server. Returns an integer with the number of new posts.
            :param: force: Force thread update.
            :return: int: How many new posts fetched.
        """

        # The thread has already 404'ed, this function shouldn't do anything anymore.
        if self.is_404 and not force:
            return 0

        url = '%s/%s' % (self._board._base_url, _THREAD % (self._board._board_name, self.id))
        if self._last_modified:
            headers = {'If-Modified-Since': self._last_modified}
        else:
            headers = None

        res = self._board._session.get(url, headers=headers)

        # 304 Not Modified, no new posts.
        if res.status_code == 304:
            return 0

        # 404 Not Found, thread died.
        elif res.status_code == 404:
            self.is_404 = True
            # remove post from cache, because it's gone.
            self._board._thread_cache.pop(self.id, None)
            return 0


        elif res.status_code == 200:
            # If we somehow 404'ed, we should put ourself back in the cache.
            if self.is_404:
                self.is_404 = False
                self._board._thread_cache[self.id] = self

            # Remove
            self.want_update = False
            self.omitted_images = 0
            self.omitted_posts = 0

            self._last_modified = res.headers['last-modified']
            posts = res.json['posts']

            originalPostCount = len(self.replies)
            self.topic = Post(self, posts[0])
            if self.last_reply_id and not force:
                self.replies.extend(Post(self, p) for p in posts if p['no'] > self.last_reply_id)
            else:
                self.replies[:] = [Post(self, p) for p in posts[1:]]
            newPostCount = len(self.replies)
            postCountDelta = newPostCount - originalPostCount
            if not postCountDelta:
                return 0

            self.last_reply_id = self.replies[-1].post_number

            return postCountDelta

        else:
            res.raise_for_status()

    def __repr__(self):
        extra = ""
        if self.omitted_images or self.omitted_posts:
            extra = ", %i omitted images, %i omitted posts" % (
                self.omitted_images, self.omitted_posts
            )

        return '<Thread /%s/%i, %i replies%s>' % (
            self._board.name, self.id, len(self.replies), extra
        )


class Post(object):
    def __init__(self, thread, data):
        self._thread = thread
        self._data = data

    @property
    def post_number(self):
        """
            :return: int
        """
        return self._data['no']

    @property
    def post_id(self):
        """
            :return: int
        """
        return self._data.get('id')

    @property
    def name(self):
        return self._data.get('name', None)

    @property
    def email(self):
        return self._data.get('email', None)

    @property
    def tripcode(self):
        return self._data.get('tripcode', None)

    @property
    def subject(self):
        return self._data.get('sub', None)

    @property
    def comment(self):
        return self._data.get('com', None)

    @property
    def timestamp(self):
        return self._data.get('time')

    @property
    def datetime(self):
        return datetime.fromtimestamp(self._data['time'])

    @property
    def file_md5(self):
        if not self.has_file:
            return None

        return base64.b64decode(self._data['md5'])

    @property
    def file_md5_hex(self):
        if not self.has_file:
            return None

        return self.file_md5.encode('hex')

    @property
    def file_url(self):
        if not self.has_file:
            return None

        board = self._thread._board

        return '%s://%s/%s/src/%i%s' % (
            'https' if board._https else 'http',
            _4CHAN_IMAGES_URL,
            board.name,
            self._data['tim'],
            self._data['ext']
        )

    @property
    def file_extension(self):
        return self._data.get('ext', None)

    @property
    def file_size(self):
        return self._data.get('fsize', None)

    @property
    def file_width(self):
        return self._data.get('w', None)

    @property
    def file_height(self):
        return self._data.get('h', None)

    @property
    def file_deleted(self):
        return self._data.get('filedeleted', 0) == 1

    @property
    def thumbnail_width(self):
        return self._data.get('tn_w', None)

    @property
    def thumbnail_height(self):
        return self._data.get('tn_h', None)

    @property
    def thumbnail_url(self):
        if not self.has_file:
            return None

        board = self._thread._board

        return '%s://%s/%s/thumb/%is.jpg' % (
            'https' if board._https else 'http',
            _4CHAN_THUMBS_URL,
            board.name,
            self._data['tim']
        )

    @property
    def has_file(self):
        return 'filename' in self._data

    def file_request(self):
        return self._thread._board._session.get(self.file_url)

    def thumbnail_request(self):
        return self._thread._board._session.get(self.thumbnail_url)

    @property
    def post_url(self):
        board = self._thread._board
        return "%s://%s/%s/res/%i#p%i" % (
            'https' if board._https else 'http',
            _4CHAN_BOARDS_URL,
            board.name,
            self._thread.id,
            self.post_number
        )

    def __repr__(self):
        return "<Post /%s/%i#%i, hasFile: %r>" % (
            self._thread._board.name,
            self._thread.id,
            self.post_number,
            self.has_file
        )
