#!/usr/bin/env python3

import sys
sys.path.insert(0, '../')

import py4chan

def main():
    v = py4chan.Board('v')
    thread = v.get_thread(152900882)
    print(thread)
    print('sticky?', thread.sticky)
    print('closed?', thread.closed)
    topic = thread.topic
    print('Topic Repr', topic)
    print('post_number', topic.post_number)
    print('timestamp', topic.timestamp)
    print('DateTime', repr(topic.datetime))
    print('file_md5_hex', topic.file_md5_hex)
    print('file_url', topic.file_url)
    print('subject', topic.subject)
    print('comment', topic.comment)
    print('thumbnail_url',topic.thumbnail_url)
    print('Replies', thread.replies)


if __name__ == '__main__':
    main()
